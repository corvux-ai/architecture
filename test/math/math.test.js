const assert = require('assert');
const {name, operation} = require('../../math');

describe('#Functions', () => {
  it('The operation should be 4', () => {
    assert.equal(4, name(2,2));
  });
  it('The operation should be 8', () => {
    assert.equal(8, name(6, 2));
  });
  it('The operation should be 10', () => {
    assert.equal(10, name(2, 8));
  });
  it('The operation should be 100', () => {
    assert.equal(100, name(2, 98));
  });
  it('The operation should be 200', () => {
    assert.equal(200, name(2, 198));
  });
  it('The operation should be 201', () => {
    assert.equal(201, name(3, 198));
  });
  describe('#oter', () => {
    it ('operation', () => {
      assert.equal(7, operation());
    })
  });
});
